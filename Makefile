LDFLAGS = "-w -s -X acs/comet/config.buildstamp=`date '+%Y-%m-%d_%I:%M:%S'` -X acs/comet/config.githash=`git rev-parse HEAD`"

build:build_comet build_dispatcher build_management

build_comet:
	cd comet/cmd/ && GOOS=linux go build -ldflags ${LDFLAGS} -o comet

build_dispatcher:
	cd dispatcher/cmd/ && GOOS=linux go build -ldflags ${LDFLAGS} -o dispatcher

build_management:
	cd management && GOOS=linux go build -ldflags ${LDFLAGS} -o management

tar:build
	mkdir bin
	cp comet/cmd/comet bin
	cp dispatcher/cmd/dispatcher bin
	cp management/management bin
	tar -cvf acs.tar.gz bin

clean:
	@rm -rf comet/cmd/comet dispatcher/cmd/dispatcher management/management

default:build
