package main

import (

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"fmt"
	"acs/pbmodel"

)

// TaskType to specify the task types.
type TaskType int

const (
	// TaskTypePatch to identify the patch task.
	TaskTypePatch TaskType = iota
	// TaskTypeSchema to identify the schema task.
	TaskTypeSchema
	//TaskTypeEvent 推送的event
	TaskTypeEvent
)
// AppPlatform defines which platform the app runs on
type AppPlatform int32

const (
	// DbCollectionUserTasks is the storage collection name of "user tasks"
	DbCollectionUserTasks = "user_tasks"
	// DbCollectionTaskContents is the storage collection name of "task contents"
	DbCollectionTaskContents = "task_contents"
)

const (
	URL = "127.0.0.1:27017"
)


// UserTaskPersist the task that to be persisted in solid storage for the user.
type UserTaskPersist struct {
	Type     TaskType
	BundleID string
	Platform AppPlatform
	UID      string
	TaskNo   int64
	Status   int
	UUID     string

	//过滤条件
	Appver [2]string
	BundleIds []string
	System []int
	OSVersion [2]string
	Brand []string
	Model []string
}
type EventTaskPersist struct {
	Type   TaskType
	TaskNo int64
	// Content is one of the task type structs.
	Content *EventTaskToStore
}
type EventTaskToStore struct {
	pbmodel.Event
	Expiration int64  //过期时间
	AllSend int //是否是*, 1是全部发送，0不是全部发送
}





func main() {

	session, err := mgo.Dial(URL)  //连接数据库
	if err != nil {
		panic(err)
	}
	defer session.Close()
	session.SetMode(mgo.Monotonic, true)

	ses := session.DB("acs_dev")



	userTaskPersist := new(UserTaskPersist)
	err = ses.C(DbCollectionUserTasks).Find(bson.M{"uid": "111", "type": TaskTypeEvent, "taskno":13391759256049369, "status": 1}).One(userTaskPersist)


fmt.Println(userTaskPersist, "===============")

	////db.getCollection('user_tasks').find({'appver.0':{$lte:'3.0.0'}})
	//
	////匹配当前用户未发送成功的任务
	//var userTaskForAll []UserTaskPersist
	//err = ses.C(DbCollectionUserTasks).Find(bson.M{
	//	"uid": "112",
	//	"type": TaskTypeEvent,
	//	"status": 0,
	//	"appver.0":bson.M{"$lte": "3.0.0"}, //<=
	//	"appver.1":bson.M{"$gte": "3.0.0"}, //>=
	//	"bundleids":bson.M{"$in": []string{"com.jumei.live.android"}},
	//	"system":bson.M{"$in": []int{1}},
	//	"osversion.0":bson.M{"$lte": "11.2"}, //<=
	//	"osversion.1":bson.M{"$gte": "3.0.0"}, //>=
	//
	//}).All(&userTaskForAll)
	//if err != nil {
	//	return
	//}
	////当前未发送消息的id
	//for _, v := range  userTaskForAll {
	//	fmt.Println(v, "=====")
	//	eventTaskPersist := new(EventTaskPersist)
	//	err = ses.C(DbCollectionTaskContents).Find(bson.M{"type": TaskTypeEvent, "taskno":v.TaskNo, "content.status": 0, "content.expiration":bson.M{"$gt": time.Now().Unix()} }).One(eventTaskPersist)
	//	if err != nil {
	//		return
	//	}
	//	//logger.Debugf("sending event task[%v] to client[%v:%v]...",  eventTaskPersist.TaskNo, userInfo.UUID, userInfo.UID)
	//	//eventResp := c.SendCmd(proto.CmdEvent, &eventTaskPersist.Content.Event, &pbmodel.EventResp{})
	//	//if eventResp.Err != nil {
	//	//	logger.Warnf("Failed to send cmd[%v] to client [%v:%v], Err: %v", proto.CmdEvent, userInfo.UUID, userInfo.UID, eventResp.Err)
	//	//}else {
	//	//	//将当前未发送状态改为已发送
	//	//	err := ses.C(DbCollectionUserTasks).Update(bson.M{"uid": userInfo.UID, "type": TaskTypeEvent, "taskno":v.TaskNo}, bson.M{"$set": bson.M{"status": 1}})
	//	//	if err != nil {
	//	//		//TODO 如果更新不成功可能会造成重复发送的情况
	//	//		logger.Warnf("Failed to remove eventTask[%v] status: %v Err: %v",  userInfo.UID, 0, eventResp.Err)
	//	//		return
	//	//	}
	//	//}
	//}
	//




















}
