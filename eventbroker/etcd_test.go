package eventbroker

import (
	"testing"
	"time"
)

func TestEtcd(t *testing.T) {
	cfg := Config{
		Endpoints: []string{"http://127.0.0.1:2379"},
		// set timeout per request to fail fast when the target endpoint is unavailable
	}

	client, err := NewClient(cfg)
	if err != nil {
		t.Error(err)
	}
	client.Set("/nodes/1234", "1234")
	client.Set("/nodes/1235", "1235")
	client.Set("/nodes/1236", "1236")
	time.Sleep(1 * time.Second)
	values, err := client.GetNodes("/nodes")
	if err != nil {
		t.Error(err)
	}
	t.Log(values)
}
