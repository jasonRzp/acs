package models

import (
	"net/http"

	"github.com/labstack/echo"
)

//当前登录用户 (session 使用)
const (
	//当前登录用户 (session 使用)
	SESSION_MANAGER    string = "CurrentManager"
	SESSION_USER       string = "CurrentUser"
	SESSION_USER_TOKEN string = "user_token"
	TimeFormat         string = "2006-01-02 15:04:05"

	//API_URL              string = "http://auth.jumeird.com"
	API_LOGIN_URL        string = "/api/login/"
	API_MEMBER_URL       string = "/api/member/"
	API_GROUP_MEMBER_URL string = "/api/groupmembers/"
	API_GROUP_URL        string = "/api/groups/"

	VIEW_LOGIN_URL string = "/login.html"
	VIEW_INDEX_URL string = "/index.html"
	//acs 系统的key
//	APP_KEY string = "17aba3e431d211e68d73842b2b738d12"
)
const (
	Right_Status  = 200
	ACCESS_DENIED = 1005
	VALUE_EXISTS  = 2007
	NOT_FOUND     = 4001
	SERVER_ERROR  = 5001

	//---------------日志类型---
	LOG_TYPE_USER              int = 1
	LOG_TYPE_APP               int = 2
	LOG_TYPE_PATCH             int = 3
	LOG_TYPE_API_USER          int = 4
	LOG_TYPE_API_URL           int = 5
	LOG_TYPE_USER_PRIVILEGE    int = 6
	LOG_TYPE_PUSH_PATCH_SCHEMA int = 7
	LOG_TYPE_PUSH_EVENT        int = 8
	LOG_TYPE_BASIC_INFO        int = 9
)

var _errors = map[int]string{
	4001: "not found",
	5001: "server error",
}

func ExportJson(reusltCode int, title, Content interface{}, totalCount int64) map[string]interface{} {

	if reusltCode == Right_Status {
		//如果是正常的输出
		var resultMap map[string]interface{} = map[string]interface{}{
			"reusltCode":   Right_Status,
			"reusltTitle":  title,
			"reusltDetail": Content,
		}
		if totalCount >= 0 {
			resultMap["totalCount"] = totalCount
		}

		return resultMap
	} else {
		//如果是明确的错误输出
		reusltTitle := _errors[reusltCode]
		return map[string]interface{}{
			"reusltCode":   reusltCode,
			"reusltTitle":  reusltTitle,
			"reusltDetail": Content,
		}
	}
	//如果是异常的错误输出
	return map[string]interface{}{
		"reusltCode":   SERVER_ERROR,
		"reusltTitle":  "unknown error occured",
		"reusltDetail": "unknown error occured",
	}
}
func ErrorHandler(c echo.Context) {
	if errstr := recover(); errstr != nil {
		err := errstr.(error)
		c.JSON(http.StatusOK, err.Error())
	}
}
func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}
