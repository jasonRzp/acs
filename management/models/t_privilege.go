package models

import (
	"errors"
	"fmt"
	"strings"

	"github.com/astaxie/beego/orm"
)

type TPrivilege struct {
	Id          int64  `orm:"column(id);auto" description:""  form:"id"  json:"id"`
	Username    string `orm:"column(username);size(45)" description:""  form:"username"  json:"username"`
	TargetTable string `orm:"column(target_table);size(45);null" description:"操作的数据表"  form:"target_table"  json:"target_table"`
	TargetRow   int    `orm:"column(target_row);null" description:"操作的数据"  form:"target_row"  json:"target_row"`
	Action      string `orm:"column(action);size(15);null" description:"操作的内容。空白 为全部允许，create，read，update，delete"  form:"action"  json:"action"`
}

func (t *TPrivilege) TableName() string {
	return "t_privilege"
}
func (t *TPrivilege) TableComment() string {
	return ""
}

func init() {
	orm.RegisterModel(new(TPrivilege))
}

// AddTPrivilege insert a new TPrivilege into database and returns
// last inserted Id on success.
func AddTPrivilege(m *TPrivilege) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetTPrivilegeById retrieves TPrivilege by Id. Returns error if
// Id doesn't exist
func GetTPrivilegeById(id int64) (v *TPrivilege, err error) {
	o := orm.NewOrm()
	v = &TPrivilege{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllTPrivilege retrieves all TPrivilege matches certain condition. Returns empty list if
// no records exist
func GetAllTPrivilege(query map[string]interface{}, fields []string, sortby []string, order []string,
	offset int64, limit int64, needCount bool) (ml []TPrivilege, totalCount int64, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(TPrivilege))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, 0, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, 0, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, 0, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, 0, errors.New("Error: unused 'order' fields")
		}
	}

	var l []TPrivilege
	qs = qs.OrderBy(sortFields...)
	if needCount == true {
		if totalCount, err = qs.Count(); err != nil {

			return nil, 0, err
		}
	}
	if limit > 0 {
		qs = qs.Limit(limit, offset)
	}
	if _, err = qs.All(&l); err == nil {

		return l, totalCount, nil
	}
	return nil, totalCount, err
}

// UpdateTPrivilege updates TPrivilege by Id and returns error if
// the record to be updated doesn't exist
func UpdateTPrivilegeById(m *TPrivilege) (vo *TPrivilege, err error) {
	o := orm.NewOrm()
	v := TPrivilege{Id: m.Id}
	// ascertain id exists in the database
	//if err = o.Read(&v); err == nil {
	//var num int64
	if _, err = o.Update(m); err == nil {
		return m, nil
		//	fmt.Println("Number of records updated in database:", num)
	}
	//}
	return &v, nil
}

// DeleteTPrivilege deletes TPrivilege by Id and returns error if
// the record to be deleted doesn't exist
func DeleteTPrivilege(id int64) (err error) {
	o := orm.NewOrm()
	v := TPrivilege{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&TPrivilege{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
