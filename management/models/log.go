package models

import (
	"strconv"
	"time"
)

const (
	// 安全日志，包括登录。退出等
	SecuritLog int = 1
)

func AppendLog(user *UserWithPirvilege, message string, logType int) error {
	var log TLog
	log.UserNickname = user.Username
	log.LogType = strconv.Itoa(logType)
	log.Content = user.Department + "(" + user.Fullname + ")操作:" + message
	log.Logtime = time.Now()
	_, err := AddTLog(&log)
	return err
}
