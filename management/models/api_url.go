package models

import "errors"

var apiUrlMap map[string]*TApiUrl = make(map[string]*TApiUrl)

func LoadAllApiUrl() error {
	allApiUrllist, _, err := GetAllTApiUrl(nil, nil, nil, nil, 0, 10000, false)

	for key, _ := range apiUrlMap {
		delete(apiUrlMap, key)
	}
	for _, apiurlItem := range allApiUrllist {
		var apiurl TApiUrl
		apiurl.Url = apiurlItem.Url
		apiurl.Id = apiurlItem.Id
		apiurl.Label = apiurlItem.Label
		apiurl.Enable = apiurlItem.Enable
		apiUrlMap[apiurlItem.Url] = &apiurl
	}
	return err
}
func CheckApiUrlEnable(path string) bool {
	if apiUrlMap[path] == nil {
		return false
	} else {
		return apiUrlMap[path].Enable == 1
	}
	return false
}

func AppendApiUrl2Memery(newItem *TApiUrl) {
	apiUrlMap[newItem.Url] = newItem
}

func GetApiUrlByApiUrlId(apiUrlId int) (*TApiUrl, error) {
	for key, _ := range apiUrlMap {
		if apiUrlMap[key].Id == int64(apiUrlId) {
			return apiUrlMap[key], nil
		}
	}
	return nil, errors.New("没有这个API")
}
