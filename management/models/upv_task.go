package models

import "gopkg.in/mgo.v2/bson"

type UpvTask struct {
	ID      bson.ObjectId  `json:"_id" bson:"_id"`
	Enable  int            `json:"enable" bson:"enable"`
	Type    int32          `json:"type" bson:"type"`
	Taskno  int64          `json:"taskno" bson:"taskno"`
	Content upvTaskContent `json:"content" bson:"content"`
}
type upvTaskContent struct {
	Bundleid     string   `json:"bundleid" bson:"bundleid"`
	Platform     string   `json:"platform" bson:"platform"`
	Prevpatchid  string   `json:"prevpatchid,omitempty" bson:"prevpatchid,omitempty"`
	prevschemaid string   `json:"prevschemaid,omitempty" bson:"prevschemaid,omitempty"`
	Patch        upvPatch `json:"patch,omitempty" bson:"patch,omitempty"`
	Schema       upvPatch `json:"schema,omitempty" bson:"schema,omitempty"`
}

type upvPatch struct {
	Schemaid string `bson:"schemaid,omitempty"  json:"schemaid,omitempty"`
	Patchid  string `bson:"patchid,omitempty"  json:"patchid,omitempty"`
	URL      string `bson:"url"  json:"url"`
	Md5      string `bson:"md5"  json:"md5"`
	Desc     string `bson:"desc"  json:"desc"`
	Size     int64  `bson:"size,omitempty"  json:"size,omitempty"`
}

type UpvUserTask struct {
	ID       bson.ObjectId `json:"_id" bson:"_id"`
	UID      string        `json:"uid" bson:"uid"`
	Taskno   int64         `json:"taskno" bson:"taskno"`
	Platform int32         `json:"platform" bson:"platform"`
	Bundleid string        `json:"bundleid" bson:"bundleid"`
	Enable   int           `json:"enable" bson:"enable"`
}

type UpvUserTaskList struct {
	UPVList []UpvUserTask `json:"upvlist" bson:"upvlist"`
}
