package models

import (
	"errors"
	"fmt"
	"strings"

	"github.com/astaxie/beego/orm"
)

type TClientApp struct {
	Id       int64  `orm:"column(id);auto" description:""  form:"id"  json:"id"`
	ClientId string `orm:"column(client_id);size(45);null" description:""  form:"client_id"  json:"client_id"`
	Platform string `orm:"column(platform);size(45);null" description:"客户的系统平台"  form:"platform"  json:"platform"`
	AppId    int    `orm:"column(app_id);null" description:"客户安装的app"  form:"app_id"  json:"app_id"`
	Lpid     string `orm:"column(lpid);size(45);null" description:"last patch id"  form:"lpid"  json:"lpid"`
	AppName  string `orm:"column(app_name);size(45);null" description:"app 的名称"  form:"app_name"  json:"app_name"`
	Enable   int8   `orm:"column(enable);null" description:"1 为可用，0 为删除。默认为1"  form:"enable"  json:"enable"`
}

func (t *TClientApp) TableName() string {
	return "t_client_app"
}
func (t *TClientApp) TableComment() string {
	return "用户持有的app"
}

func init() {
	orm.RegisterModel(new(TClientApp))
}

// AddTClientApp insert a new TClientApp into database and returns
// last inserted Id on success.
func AddTClientApp(m *TClientApp) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetTClientAppById retrieves TClientApp by Id. Returns error if
// Id doesn't exist
func GetTClientAppById(id int64) (v *TClientApp, err error) {
	o := orm.NewOrm()
	v = &TClientApp{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllTClientApp retrieves all TClientApp matches certain condition. Returns empty list if
// no records exist
func GetAllTClientApp(query map[string]interface{}, fields []string, sortby []string, order []string,
	offset int64, limit int64, needCount bool) (ml []TClientApp, totalCount int64, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(TClientApp))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, 0, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, 0, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, 0, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, 0, errors.New("Error: unused 'order' fields")
		}
	}

	var l []TClientApp
	qs = qs.OrderBy(sortFields...)
	if needCount == true {
		if totalCount, err = qs.Count(); err != nil {

			return nil, 0, err
		}
	}
	if _, err = qs.Limit(limit, offset).All(&l); err == nil {

		return l, totalCount, nil
	}
	return nil, totalCount, err
}

// UpdateTClientApp updates TClientApp by Id and returns error if
// the record to be updated doesn't exist
func UpdateTClientAppById(m *TClientApp) (vo *TClientApp, err error) {
	o := orm.NewOrm()
	v := TClientApp{Id: m.Id}
	// ascertain id exists in the database
	//if err = o.Read(&v); err == nil {
	//var num int64
	if _, err = o.Update(m); err == nil {
		return m, nil
		//	fmt.Println("Number of records updated in database:", num)
	}
	//}
	return &v, nil
}

// DeleteTClientApp deletes TClientApp by Id and returns error if
// the record to be deleted doesn't exist
func DeleteTClientApp(id int64) (err error) {
	o := orm.NewOrm()
	v := TClientApp{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&TClientApp{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
