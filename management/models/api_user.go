package models

//allApiUserTokenMap key 是token ，value目前是username 。
var allApiUserTokenMap map[string]string = make(map[string]string)

//根据账户提供的toke来判断该用户是否具有权限访问该接口。
func CheckApiUser(userToken string) bool {
	if allApiUserTokenMap[userToken] != "" {
		return true
	}
	return false
}
func LoadAllApiUser() error {
	apiUserList, _, err := GetAllTApiUser(nil, nil, nil, nil, 0, 10000, false)

	for key, _ := range allApiUserTokenMap {
		delete(allApiUserTokenMap, key)
	}
	for _, apiuser := range apiUserList {
		if apiuser.Enable == 1 {

			allApiUserTokenMap[apiuser.Token] = apiuser.UserName
		}
	}
	return err
}

//CheckHasAPIUser 检查一下是否有这个用户名存在了。 存在则返回true 否则返回false
func CheckHasAPIUser(username string) bool {
	for _, existUsername := range allApiUserTokenMap {
		if existUsername == username {
			return true
		}
	}
	return false
}
func GetTokenByName(username string) string {
	for token, existUsername := range allApiUserTokenMap {
		if existUsername == username {
			return token
		}
	}
	return ""
}
func PushAPIUser(username string, token string, kill bool) {
	delete(allApiUserTokenMap, GetTokenByName(username))
	if kill == false {
		allApiUserTokenMap[token] = username
	}
}
