package models

import (
	"errors"
	"fmt"
	"strings"

	"github.com/astaxie/beego/orm"
)

type TApiUserUrl struct {
	Id        int64  `orm:"column(id);auto" description:""  form:"id"  json:"id"`
	UserToken string `orm:"column(user_token);null" description:""  form:"user_token"  json:"user_token"`
	ApiUrl    string `orm:"column(api_url);null" description:""  form:"api_url"  json:"api_url"`
	Enable    int8   `orm:"column(enable);null" description:""  form:"enable"  json:"enable"`
}

func (t *TApiUserUrl) TableName() string {
	return "t_api_user_url"
}
func (t *TApiUserUrl) TableComment() string {
	return "账户和api的对应关系表"
}

func init() {
	orm.RegisterModel(new(TApiUserUrl))
}

// AddTApiUserUrl insert a new TApiUserUrl into database and returns
// last inserted Id on success.
func AddTApiUserUrl(m *TApiUserUrl) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetTApiUserUrlById retrieves TApiUserUrl by Id. Returns error if
// Id doesn't exist
func GetTApiUserUrlById(id int64) (v *TApiUserUrl, err error) {
	o := orm.NewOrm()
	v = &TApiUserUrl{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllTApiUserUrl retrieves all TApiUserUrl matches certain condition. Returns empty list if
// no records exist
func GetAllTApiUserUrl(query map[string]interface{}, fields []string, sortby []string, order []string,
	offset int64, limit int64, needCount bool) (ml []TApiUserUrl, totalCount int64, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(TApiUserUrl))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, 0, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, 0, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, 0, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, 0, errors.New("Error: unused 'order' fields")
		}
	}

	var l []TApiUserUrl
	qs = qs.OrderBy(sortFields...)
	if needCount == true {
		if totalCount, err = qs.Count(); err != nil {

			return nil, 0, err
		}
	}
	if _, err = qs.Limit(limit, offset).All(&l); err == nil {

		return l, totalCount, nil
	}
	return nil, totalCount, err
}

// UpdateTApiUserUrl updates TApiUserUrl by Id and returns error if
// the record to be updated doesn't exist
func UpdateTApiUserUrlById(m *TApiUserUrl) (vo *TApiUserUrl, err error) {
	o := orm.NewOrm()
	v := TApiUserUrl{Id: m.Id}
	// ascertain id exists in the database
	//if err = o.Read(&v); err == nil {
	//var num int64
	if _, err = o.Update(m); err == nil {
		return m, nil
		//	fmt.Println("Number of records updated in database:", num)
	}
	//}
	return &v, nil
}

// DeleteTApiUserUrl deletes TApiUserUrl by Id and returns error if
// the record to be deleted doesn't exist
func DeleteTApiUserUrl(id int64) (err error) {
	o := orm.NewOrm()
	v := TApiUserUrl{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&TApiUserUrl{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
