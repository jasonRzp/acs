package controllers

import (
	"acs/management/models"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"sync"

	"github.com/chaosue/echo-sessions"
	"github.com/labstack/echo"
)

//GetApiUserListByAPIHandler 添加
func GetApiUserListByAPIHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)
	api_url_idstr := c.Param("api_url_id")
	api_url_id, idErr := strconv.Atoi(api_url_idstr)
	models.CheckError(idErr)
	currentApiUrl, apiErr := models.GetApiUrlByApiUrlId(api_url_id)
	models.CheckError(apiErr)
	apiurllist := models.GetUserListByApiUrl(currentApiUrl.Url)
	c.JSON(http.StatusOK, apiurllist)
	return
}

type apiUserUrlObj struct {
	AddApiUserUrllist []*models.TApiUserUrl `json:"add_api_user_urllist"`
	CutApiUserUrllist []*models.TApiUserUrl `json:"cutApi_user_urllist"`
}

//
func SaveApiUserUrlByUserHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)
	session := sessions.Default(c)
	manager := session.Get(models.SESSION_USER)
	if manager == nil {
		err = c.Redirect(http.StatusTemporaryRedirect, models.VIEW_LOGIN_URL)
		models.CheckError(err)
	}
	//	currentUser := models.CheckLogin(c)
	api_user_idsTr := c.Param("api_user_token")
	requestbody, requesterr := ioutil.ReadAll(c.Request().Body)
	models.CheckError(requesterr)
	var apiUserUrlobj apiUserUrlObj
	err = json.Unmarshal(requestbody, &apiUserUrlobj)
	models.CheckError(err)
	adduserStr := ""
	for _, item := range apiUserUrlobj.AddApiUserUrllist {
		item.UserToken = api_user_idsTr
		adduserStr += item.UserToken
	}
	cutuserStr := ""
	for _, item := range apiUserUrlobj.CutApiUserUrllist {
		item.UserToken = api_user_idsTr
		cutuserStr += item.UserToken
	}
	var sy sync.Mutex
	sy.Lock()
	err = models.BatchInsertAPIUserUrl(apiUserUrlobj.AddApiUserUrllist)
	models.CheckError(err)
	err = models.BatchRemoveAPIUserUrlByUser(api_user_idsTr, apiUserUrlobj.CutApiUserUrllist)
	models.CheckError(err)
	sy.Unlock()
	models.LoadAllApiUserUrl()
	c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, nil, models.GetAllApiUserUrlList(), -1))
	return
}

func SaveApiUserUrlByURLHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)
	session := sessions.Default(c)
	manager := session.Get(models.SESSION_USER)
	if manager == nil {
		err = c.Redirect(http.StatusTemporaryRedirect, models.VIEW_LOGIN_URL)
		models.CheckError(err)
	}
	currentUser := models.CheckLogin(c)
	api_url_idstr := c.Param("api_url_id")
	api_url_id, idErr := strconv.Atoi(api_url_idstr)
	models.CheckError(idErr)
	currentApiUrl, apiErr := models.GetApiUrlByApiUrlId(api_url_id)
	models.CheckError(apiErr)
	requestbody, requesterr := ioutil.ReadAll(c.Request().Body)
	models.CheckError(requesterr)
	var apiUserUrlobj apiUserUrlObj
	err = json.Unmarshal(requestbody, &apiUserUrlobj)
	models.CheckError(err)
	adduserStr := ""
	for _, item := range apiUserUrlobj.AddApiUserUrllist {
		item.ApiUrl = currentApiUrl.Url
		adduserStr += item.UserToken
	}
	cutuserStr := ""
	for _, item := range apiUserUrlobj.CutApiUserUrllist {
		item.ApiUrl = currentApiUrl.Url
		cutuserStr += item.UserToken
	}
	err = models.BatchInsertAPIUserUrl(apiUserUrlobj.AddApiUserUrllist)
	models.CheckError(err)
	err = models.BatchRemoveAPIUserUrl(currentApiUrl.Url, apiUserUrlobj.CutApiUserUrllist)
	models.CheckError(err)
	models.LoadAllApiUserUrl()
	models.AppendLog(&currentUser, "修改api:"+api_url_idstr+"添加访问者:"+adduserStr+"取消访问者:"+cutuserStr, models.LOG_TYPE_API_URL)

	c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, nil, models.GetAllApiUserUrlList(), -1))
	return
}
