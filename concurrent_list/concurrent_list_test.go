package clist

import (
	"sort"
	"strconv"
	"testing"
)

func TestCreate(t *testing.T) {
	l := New(10)
	if l == nil {
		t.Error("list is null")
	}

	if l.Count() != 0 {
		t.Error("list count should be 0")
	}
}

func TestAppend(t *testing.T) {
	l := New(10)
	l.Append("103499196", "yangxin")
	l.Append("97852745", "pastor")
	if l.Count() != 2 {
		t.Error("list should contain two elements")
	}
}

func TestRemove(t *testing.T) {
	l := New(10)
	e1 := l.Append("103499196", "yangxin")
	e2 := l.Append("97852745", "pastor")
	l.Remove("103499196", e1)
	if l.Count() != 1 {
		t.Error("list should contain one element")
	}
	l.Remove("97852745", e2)
	if l.Count() != 0 {
		t.Error("list should contain 0 element")
	}
}

func TestCount(t *testing.T) {
	l := New(10)
	key := "97852745"
	for i := 0; i < 100; i++ {
		l.Append(key, i)
	}
	if l.Count() != 100 {
		t.Error("list count should be 100")
	}
}

func TestIter(t *testing.T) {
	l := New(10)
	key := "97852745"
	for i := 0; i < 100; i++ {
		l.Append(key, i)
	}
	count := 0
	for e := range l.Iter() {
		if e == nil {
			t.Error("expecting an object")
		}
		count++
	}
	if count != 100 {
		t.Error("we should have 100 elements")
	}
}

func TestIterBuffered(t *testing.T) {
	l := New(10)
	key := "97852745"
	for i := 0; i < 100; i++ {
		l.Append(key, i)
	}
	count := 0
	for e := range l.IterBuffered() {
		if e == nil {
			t.Error("expecting an object")
		}
		count++
	}
	if count != 100 {
		t.Error("we should have 100 elements")
	}
}

func TestConcurrent(t *testing.T) {
	m := New(10)
	ch := make(chan int)
	const iterations = 1000
	var a [iterations]int

	go func() {
		for i := 0; i < iterations/2; i++ {
			e := m.Append(strconv.Itoa(i), i)
			val := m.Remove(strconv.Itoa(i), e)
			ch <- val.(int)
		}
	}()

	go func() {
		for i := iterations / 2; i < iterations; i++ {
			e := m.Append(strconv.Itoa(i), i)
			val := m.Remove(strconv.Itoa(i), e)
			ch <- val.(int)
		}
	}()

	counter := 0
	for elem := range ch {
		a[counter] = elem
		counter++
		if counter == iterations {
			break
		}
	}

	sort.Ints(a[0:iterations])

	if m.Count() != 0 {
		t.Error("Expecting 0 elements.")
	}

	for i := 0; i < iterations; i++ {
		if i != a[i] {
			t.Error("missing value", i)
		}
	}
}
