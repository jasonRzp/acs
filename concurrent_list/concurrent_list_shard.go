package clist

import (
	"container/list"
	"sync"
)

type ConcurrentListShard struct {
	items *list.List
	sync.RWMutex
}

func (this *ConcurrentListShard) Count() int {
	this.RLock()
	defer this.RUnlock()
	return this.items.Len()
}

func (this *ConcurrentListShard) GetItems() *list.List {
	this.RLock()
	defer this.RUnlock()
	return this.items
}
