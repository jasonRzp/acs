package servernode

type Node struct {
	Ip       string   `json:"ip"`
	BindAddr string   `json:"bind"`
	Info     NodeInfo `json:"info"`
}

type NodeInfo struct {
	RegTime    uint64      `json:"regTime"`
	UpdateTime uint64      `json:"update_time"`
	Mem        MemInfo     `json:"mem"`
	Io         float64     `json:"io"`
	Load       LoadAverage `json:"load"`
}

type MemInfo struct {
	Total      uint64
	Used       uint64
	Free       uint64
	ActualFree uint64
	ActualUsed uint64
}

type LoadAverage struct {
	One, Five, Fifteen float64
}
