#/bin/bash
GOOS=linux  go build -ldflags "-w -s -X main.buildstamp=`date '+%Y-%m-%d_%I:%M:%S'` -X main.githash=`git rev-parse HEAD`" -o comet
