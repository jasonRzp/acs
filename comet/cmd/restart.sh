#!/bin/bash
ps ax | grep comet | grep config.toml | grep -v sudo |awk '{print $1}' | xargs -I {} sudo kill -9 {}
sleep 5
sudo nohup /home/jm/acs/comet/comet -c /home/jm/acs/comet/config.toml > /home/jm/logs/acs.log &