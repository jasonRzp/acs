// Package taskprocess fetch tasks from the tasks list(queue server) and processes them.
package taskprocess

import (
	"acs/comet/client"
	"acs/taskprocess/storage"
	"sync"

	log "github.com/cihub/seelog"
)

// DefaultTaskServerConnTimeout default connection timeout (in millisenconds) to the task server(queue server).
const DefaultTaskServerConnTimeout = 1500

// DefaultTaskTopicName default topic name of the task queue.
const DefaultTaskTopicName = "upv_topic"

// DefaultTaskChannelName the default channel name for the task topic.
const DefaultTaskChannelName = "notify_apps"

var logger = log.Default
var setMutex = new(sync.Mutex)

var clients *client.ClientList

var cfg *Config

// AppPlatform defines which platform the app runs on
type AppPlatform int32

const (
	// PlatformIphone app runs on iphone
	PlatformIphone AppPlatform = iota
	// PlatformAndroid app runs on android
	PlatformAndroid
	// PlatformIpad app runs on Ipad
	PlatformIpad
	// PlatformAndroidPad app runs on android pad.
	PlatformAndroidPad
)

// Config is the config object for the taskprocess module.
type Config struct {
	// Task queue server addresses, currently it should be the nsqlookupd service addresses.
	QueueLookupdAddrs []string
	TopicName         string
	ChannelName       string
	Storage           storage.Config
}

// SetLogger sets the logger for taskprocess package.
// seelog.Default it the default logger.
func SetLogger(newLogger log.LoggerInterface) {
	setMutex.Lock()
	defer setMutex.Unlock()
	logger = newLogger
}

// SetClientManager sets the client manager that contains all the client(connection) objects.
func SetClientManager(c *client.ClientList) {
	setMutex.Lock()
	defer setMutex.Unlock()
	clients = c
}
