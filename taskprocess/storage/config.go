package storage

// Config is the persistent stroage server configurations.
type Config struct {
	// Persistent storage service DSN, we use mongodb as the storage service at the moment.
	Dsn      string
	Database string
}
